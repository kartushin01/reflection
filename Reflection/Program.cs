﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization.Json;

namespace Reflection
{
    class Program
    {


        static void Main(string[] args)
        {
            F fClass = F.Get();

            //serialize 
            DateTime t1 = DateTime.Now;
            
            string strRes = "";

            object fMethod = F.Get();

            for (int i = 0; i < 10_000; i++)
            {
                strRes = fMethod.ToCsv();
            }
            
            DateTime t2 = DateTime.Now;
            Console.WriteLine($"time: {t2-t1}");

            //deserialize 
            F fRes = new F();

            DateTime t3 = DateTime.Now;

            for (int i = 0; i < 10_000; i++)
            {
                fRes = SerialozatorExt.FromCsv<F>(strRes);
            }
            
            DateTime t4 = DateTime.Now;
            Console.WriteLine($"<T> returned: typeOf: {fRes.GetType().Name}");
            Console.WriteLine($"time: {t4 - t3}");


            Console.WriteLine($"NewtonSoft SerializeObject");
            DateTime t5 = DateTime.Now;
            
            var newtonSer = "";
            
            for (int i = 0; i < 10_000; i++)
            {
                 newtonSer = JsonConvert.SerializeObject(fClass);
            }
            
            DateTime t6 = DateTime.Now;
            Console.WriteLine($"time: {t6 - t5}");
            
            Console.WriteLine($"NewtonSoft DeserializeObject");
            DateTime t7 = DateTime.Now;
            for (int i = 0; i < 10_000; i++)
            {
                var newtonDeSer = JsonConvert.DeserializeObject<F>(newtonSer);
            }
            DateTime t8 = DateTime.Now;
            Console.WriteLine($"time: {t8 - t7}");

        }

     
    }
}
