﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Reflection;

namespace Reflection
{
    public static class SerialozatorExt
    {
        public static string ToCsv(this object o)
        {

            Type t = o.GetType();
            var props = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            StringBuilder csvStr = new StringBuilder();

            foreach (var p in props)
            {
                csvStr.AppendLine($"{p.Name},{p.GetValue(o)}" );
            }

            return csvStr.ToString();

        }

        public static T FromCsv<T>(string deserializeString) where T : new()
        {
           
            T myObject = new T();
            
            Type myType = typeof(T);

            string[] split = deserializeString.Split("\r\n");

            for (int i = 0; i < split.Length; i++) 
            {
                var r = split[i].Split(",");
                
                string name = r[0];

                if (!string.IsNullOrEmpty(name)) {

                    FieldInfo field = myType.GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);

                    //Type fieldTypeName = field.FieldType;  
                   
                    string val = r[1];

                    field.SetValue(myObject, int.Parse(val));

                }
               
            }

            return myObject;
        }
    }
}
